import wx
import re
import numpy as np
import logging
import os
from os.path import join
def extract_int(string):
    """Vraca prvi int iz stringa, ako ga
    ima - u suprotnom vraca nulu!
    """
    try:
        return int(re.search(r'\d+',string).group())
    except:
        return 0 
    
def extract_name(string):
    """Vraca ime izbora
    L1010->l i tako"""
    try:
        name = re.search(r'(L|THERM|MC|T)',string).group().lower()
        assert name in ['l','t','therm','mc']
    except:
        show_error('Wrong choice format','New format? Please contact developer!')
    else:
        return name

########################################
##########POMOCNE KLASE################
class twoway_cycle():
    """Uzima iterable, i na zvanje prev i next
    vraca odgovarajuce clanove. ovo je verovatno
    moglo lepse preko nekih dekoratora, generatora
    nesto """
    def __init__(self,it):
        if not np.iterable(it):
            raise ValueError("Must be an iterable")
        
        self.log=logging.getLogger("twoway_cycle")
        self.it=it
        self.i=0
    def next(self):
        self.i = self.i+1
        self.i = 0 if self.i==len(self.it) else self.i
        return self.it[self.i]

    def prev(self):
        self.i = len(self.it) if self.i==0 else self.i
        self.i = self.i-1
        return self.it[self.i]

    def curr(self):
        self.log.debug("returning curr element:{}".format(self.it[self.i]))
        return self.it[self.i]

class MyMessageBox(wx.MessageDialog):
    
    def __init__(self,message,title,style):
        wx.MessageBox.__init__(self,None,message,title,style)
        self.issue_button = wx.Button(self,-1,'Report Issue')
        self.Bind(wx.EVT_BUTTON,self.on_report_issue,self.issue_button)

    def on_report_issue(self,event):
        import webbrowser
        webbrowser.open('https://bitbucket.org/iTrustedYOu/tulip/issues/new')

def show_error(title,message):
    result = wx.MessageBox(message,title,style=wx.OK | wx.ICON_ERROR)


def absolute_listdir_dir(pardir,regex=None):
    """
    Given an absolute path to a directory, returns a list of
    the subdirectories contained within.
    If a regex was passed, returned directory names have to match it
    """
    
    dirs = [join(pardir,dir_) for dir_ in os.listdir(pardir)
                 if
            os.path.isdir(join(pardir, dir_))
            and
            (not regex or regex.search(dir_))]
    return dirs

def absolute_listfiles(pardir,regex=None):
    """
    Given an absolute path to direcotry, returns a list
    of files contained within that are not directories
    themselves. Ifa  regex is passed , it has to be
    matched.
    """
    files = [join(pardir,f) for f in os.listdir(pardir) if (not regex or regex.match(f))]
    return files

def open_filebrowser(path_to_dir):
    try:
        os.system('xdg-open %s' % lt_dir)
    except:
        pass

def assert_params(func):
    """
    Prima dir_,l,t,mc,therm parametre, i proverava
    da li su u ispravnom formatu. Gde da proverim
    da li su u ovom self.files?????
    """
    def wrapper(*args,**kwargs):
        regex_keys = {"dir_":re.compile(r'.*'),
                      "l":re.compile(r'L\d+'),
                      "t":re.compile(r'T\d+'),
                      "mc":re.compile(r'MC\d+'),
                      "therm":re.compile(r'THERM\d+'),
                      }
        for key,value in kwargs.iteritems():
            if key not in regex_keys.keys():
                continue
            if not regex_keys[key].match(value):
                raise NameError
        return func(*args,**kwargs)
    return wrapper




def assert_in_map(func):
    """Uvek cemo prvo imati dir_ pa l pa t pa mc pa therm"""
    def wrapper(*args,**kwargs):
        x = args[0].files
        assert x is not None
        for key in ["dir_","l","t","mc","therm"]:
            try:
                kwargs[key]
            except:
                break
            assert isinstance(x,dict), "type of x:%s" %type(x)
            assert kwargs[key] in x.keys(), "kwargs[key]:%s x.keys():%s key:%s" %(kwargs, x.keys(),key)
            x = x[kwargs[key]]
        return func(*args,**kwargs) 
    return wrapper

def assert_kwargs_decorator_maker(*expected_kwargs):
    def assert_kwargs(func):
        def wrapper(*args,**kwargs):
            for expected_kwarg in expected_kwargs:
                assert expected_kwarg in kwargs.keys() 
            return func(*args,**kwargs)
        return wrapper
    return assert_kwargs


def assert_in_bestmats(func):
    def wrapper(*args,**kwargs):
        self = args[0]
        x = self.bestmats
        assert x is not None
        for key in ["dir_","l","t"]:
            try:
                kwargs[key]
                assert kwargs[key] is not None
            except:
                break
            assert isinstance(x,dict), "type of x:%s" %type(x)
            assert kwargs[key] in x.keys(), "kwargs[key]:%s x.keys():%s key:%s" %(kwargs[key], x.keys(),key)
            x = x[kwargs[key]]
        return func(*args,**kwargs)
    return wrapper



def assert_types_decorator_maker(types):
    def assert_kwargs(func):
        def wrapper(*args,**kwargs):
            # brisemo self
            new_list = list(args)
            del new_list[0]
            for type_,arg in zip(types,new_list):
                try:
                    assert isinstance(arg,type_)
                except AssertionError:
                    raise TypeError
            return func(*args,**kwargs)
        return wrapper
    return assert_kwargs
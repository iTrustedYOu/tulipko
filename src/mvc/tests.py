from mock import MagicMock
import unittest2 as unittest 
import os
import sys
print __package__
print __name__
print __file__

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
# import mvc_test

import types
import mvc_test
from profile import *
tdf  = "/media/123AEBFC3AEBDAAD/sim_output/"

class ChoicesTest(unittest.TestCase):

    def setUp(self):
        """
        Initializes the model
        and sets the TDF path.
        Should add a list of simdirs
        for testing
        """
        tdfs = ["/media/123AEBFC3AEBDAAD/sim_output/"]
        self.choices = mvc_test.Choices()
        self.choices.simdir=tdf
        print "setup"
    
    

    def __test_dict(self,dct,meq=None,leq=None,eq=None):
        """Ovo moze nekako da se automatizuje???
        TJ, da prosledimo operator kao string???
        """
        import collections
        self.assertTrue(isinstance(dct,collections.Mapping))
        for key in dct:
            self.assertTrue(isinstance(key,basestring))
        if meq:
            self.assertTrue(len(dct.keys())>=meq,msg=str(dct))
        elif leq:
            self.assertTrue(len(dct.keys())<=leq)
        else:
            self.assertTrue(len(dct.keys())==eq)


    def test_load_choices(self):
        """Checks if structure of read
        files is a good 'choices' dictionary
        structure
        ```
        +---DIR_  
        |   +---L1
        |   |   +---T1
                |  +--['mc':str.,'therm':str]
                   |
        
        |
        ...
        ```
        """
        dct = self.choices.load_choices('bestmat.dict')
        self.assertEqual(type(dct),types.DictType)
        print dct
        if dct:
            self.__test_dict(dct,meq=1)
            for dir_ in dct.keys():
                self.__test_dict(dct[dir_],meq=1)
                for l_ in dct[dir_].keys():
                    self.__test_dict(dct[dir_][l_],meq=1)
                    for t_ in dct[dir_][l_].keys():
                        self.__test_dict(dct[dir_][l_][t_],eq=2)
                        self.assertEqual(['therm','mc'],dct[dir_][l_][t_].keys())

        print "Bestmat tests passed - you are awesome!!!"
        return True

    def test_init_model(self):
        """
        Ovde bi trebalo da je loadovano stanje
        tj. da bestmatdict ima nesto u njemu
        proverili smo da li ima odgovarajucu strukturu
        i proslom testu. i filesystem bi trebalo da je
        mapiran. Njega treba da testiram
        
        """
        self.assertTrue(self.choices.files is None and self.choices.bestmats is None)
        self.choices.init_model()
        self.assertTrue(self.choices.files and isinstance(self.choices.bestmats,dict))
        

        self.choices.map_filesystem = MagicMock()
        self.choices.init_model()
        self.choices.map_filesystem.assert_called_once_with()
        
        print "init_model tests passed - you are awesome!"

    def test_map_filesystem(self):
        """ Treba da bude odredjena struktura
        pa treba da bude, bez prljavih gluposti
        + simdir1
        |
        +-simdir2
        """
        import collections
        files = self.choices.map_filesystem()
        for simd_name,l_map in files.iteritems():
            self.assertTrue(isinstance(simd_name,basestring))
            self.__test_dict(l_map,meq=1)
            for l,t_map in l_map.iteritems():
                self.assertTrue(isinstance(l,basestring))
                self.__test_dict(t_map,meq=1)
            for t,mc_map in t_map.iteritems():
                self.assertTrue(isinstance(t,basestring))
                self.__test_dict(mc_map,meq=1)
                for mc,therm_map in mc_map.iteritems():
                    self.assertTrue(isinstance(mc,basestring))
                    self.__test_dict(l_map,meq=1)

        print "Map filesystem tests passed - you are Awesome!"

    def test_add_virtual_file(self):
        """Testira se cisto ovo spoljasnje (sve ostalo je
        u drugim funkcijama koje ova metoda poziva)
        Lako je dodavati jos test primera, tako sto
        se doda tuple jedan u test_params. 

        * files je pocetku na None
        * ako se dodaje virtualni fajl za nepostojace dir,l,t, ili therm
          doci ce do exceptiona
        * unutar funkcije se poziva create_mat sa odgovarajucim parametrima
        * ono sto create_mat vrati ce biti postavljeno kao vrednost 
        * promenjen je model - pa se notificiraju observeri, da li
          nekako da oznacim koje funkcije menjaju model?? Kako
          da to proverim??


        """

        def inner_test(dir_,l,t,mc,therm,booli):
            
            self.choices.create_mat = MagicMock(return_value=42)
            
            with self.assertRaises(TypeError):
                self.choices.add_virtual_file(dir_,l,t,mc,therm,booli)
                
            self.choices.files = dict()
            
            with self.assertRaises(KeyError):
                self.choices.add_virtual_file(dir_,l,t,mc,therm,booli)

            self.choices.files[dir_] = dict()
            with self.assertRaises(KeyError):
                self.choices.add_virtual_file(dir_,l,t,mc,therm,booli)
            
            self.choices.files[dir_][l] = dict()
            
            with self.assertRaises(KeyError):
                self.choices.add_virtual_file(dir_,l,t,mc,therm,booli)
                
            self.choices.files[dir_][l][t] = dict()
            
            with self.assertRaises(KeyError):
                self.choices.add_virtual_file(dir_,l,t,mc,therm,booli)

            self.choices.files[dir_][l][t][mc]=dict()
            self.choices.add_virtual_file(dir_,l,t,mc,therm,booli)
            self.choices.create_mat.assert_called_with(dir_,l,t,therm,booli=booli)
            self.assertTrue( self.choices.files[dir_][l][t][mc][therm]==42)
            self.choices.notify_observers = MagicMock()
            self.choices.add_virtual_file(dir_,l,t,mc,therm,booli)
            self.choices.notify_observers.assert_called_once_with()


        mc = 1000
        booli = [True for i in range(mc)]
        mc = "MC{}".format(mc)
        test_params = (("direktorijum","L5","T10000",mc,"THERM2",booli),
                       ("direktorijum2","L10","T10000",mc,"THERM100",booli))
        for params in test_params:
            inner_test(*params)
            self.choices.files=None

    def test_add_bestmat(self):
        self.choices.save_mats = MagicMock()
        
        def test_params_right(*args,**kwargs):
            with self.assertRaises(AssertionError):
                self.choices.add_bestmat(**kwargs)
            self.choices.bestmats = dict()
            with self.assertRaises(AssertionError):
                self.choices.add_bestmat(**kwargs)

            self.choices.files = {"direktorijum":{"L10":{"T10000":{"MC100":{"THERM100":None}}}}}
            self.choices.add_bestmat(**kwargs)

        def test_params_wrong(**kwargs):
           
            with self.assertRaises(BaseException):
                self.choices.add_bestmat(**kwargs)
                    
            self.choices.bestmats = dict()
            self.choices.files = {"direktorijum":{"L10":{"T10000":{"MC100":{"THERM100":None}}}}}
            with self.assertRaises(BaseException):
                self.choices.add_bestmat(**kwargs)

        wrongs =({"dir_":"direktorijum","l":"L5","t":"T10000","therm":"THERM2","mc":"MC1"},
                 {"dir_":"direktorijum","l":"L5","t":"T10000","therm":"lala","mc":"THERM2"},
                 {"dir_":"150","l":"T20","t":"T20","therm":"MC10","mc":"THERM100"},
                 {"dir_":"150","l":"L20","t":"L20","therm":"MC10","mc":"THERM100"},
                 {"dir_":"150","l":"L20","t":"T20","therm":"MC10","mc":"xx"}
        )
        rights = ({"dir_":"direktorijum","l":"L10","t":"T10000","mc":"MC100","therm":"THERM100"},)

        for ps in wrongs:
            self.choices.files =None
            test_params_wrong(**ps)



        for ps in rights:
            self.choices.files = None
            test_params_right(**ps)


    def test_best_bestmats(self):
        """
        Vazno je da nam je dir_ string,
        vazno nam je da je dir unutar files
        i da ovaj izbaci exception ako nije
        hm, ne znam da li mi defaultdict ovde
        odmaze, ok filess nam nije defaultdict
        to je dobro, ali ovi svi ostali - nece
        biti dobro!! posto nece izbaciti exception
        ajd vazi
        """
        #-- proveri da pukne ako dir_ pogresan
        # -- da pukne ako je l nebulozan
        # -- testiraj extract_int
        # -- da su mc i therm stvarno najveci
        # -- ne znam da li sam testirala ako folderi
        # su npr prazni. Sta onda uradi??? !!!!
        # sta se desi ako je foldeR prazan? - nece ga ni ubaciti
        # sta se desi ako je ltdir prazan? opet ga necemo ubacivati, ni parenta
        # ako nema nicega self.files == dict() i raisuje Exception!!
        # Znaci bar ce jedna putanja do mc therm kombinacije postojati

        with self.assertRaises(AssertionError):
            self.choices.best_bestmats(dir_="random_dir")

        self.choices.files = {"dir":{"l":{"t":{"mc":{"therm"}}}}}
        with self.assertRaises(AssertionError):
            self.choices.best_bestmats(dir_="random_dir")

        self.choices.files["random_dir"]=None
      
        with self.assertRaises(AssertionError):
            self.choices.best_bestmats(dir_="random_dir",l="random_l")

        self.choices.files["random_dir"]={"random_l":None,}

        with self.assertRaises(AttributeError):
            self.choices.best_bestmats(dir_="random_dir",l="random_l")

    def test_save_mats(self):

        prev_bestmat = self.choices.bestmats
        self.choices.save_mats()
        new_bestmat = self.choices.bestmats
        self.assertEqual(prev_bestmat,new_bestmat)
        bestmats = {"dir_":{"l":{"t":{"mc":"mc[100]","therm":"therm100"}}}}
        self.choices.bestmats = bestmats
        self.choices.save_mats()
        self.assertEqual(bestmats,self.choices.bestmats)
        import os.path
        from os.path import join
        self.assertTrue(os.path.isfile(join(self.choices.simdir,'bestmat.dict')))

    def test_remove_bestmat(self):
        # potrebno je da prosledjuje bar dir_ i l
        # ako su poslati ostali argumetni
        with self.assertRaises(AssertionError):
            self.choices.remove_bestmat()
        with self.assertRaises(AssertionError):
            self.choices.remove_bestmat(dir_="nesto")
        with self.assertRaises(AssertionError):
            self.choices.remove_bestmat(dir_="nesto",l="nesto")

        self.choices.bestmats ={"dir_":{"l":{"t":{"mc":"mc[100]","therm":"therm100"}}}}
        self.choices.remove_bestmat(dir_="dir_",l="l")
        

        self.choices.save_mats = MagicMock()
        self.choices.notify_observers = MagicMock()


        self.choices.bestmats = bestmats = {"dir_":{"l":{"t":{"mc":"mc[100]","therm":"therm100"}}}}
        self.choices.remove_bestmat(dir_="dir_",l="l")
        
        self.choices.save_mats.assert_called_once_with()
        self.choices.notify_observers.assert_called_once_with()
        
        self.choices.bestmats  = {"dir_":{"l":{"t":{"mc":"mc[100]","therm":"therm100"}}}}


    def test_add_mc(self):
        # -- Da dobavlja static mc-ove. Ne znam dal i je vazno
        # uvek da istestiram da li se pozivaju odredjene funkcije
        # Znaci mi znamo za koje parametre zadajemo mc.
        # Sigurni smo da ce se ovo prosledjivati iz comboboxova i da
        # se nece desiti da se proslede neke gluposti. je l? hm, da proveravam?

        with self.assertRaises(AssertionError):
            self.choices.add_mc("100")
        with self.assertRaises(AssertionError):
            self.choices.add_mc("100",l="L10")

        with self.assertRaises(AssertionError):
            self.choices.add_mc("100",t="T10")
            
        with self.assertRaises(NameError):
            self.choices.add_mc("100",l="zz",t="zz")

        with self.assertRaises(ValueError):
            self.choices.add_mc("100vv",l="L10",t="T10000")

        # ovaj choices ce vec raisovati neki error, hm. Ne znam da li treba da to
        # proveravam . Kako znati???
        self.choices.files = {"direktorijum":{"L10":{"T10000":{"MC100":{"THERM100":None}}}}}
        self.choices.add_mc("100",dir_="direktorijum",l="L10",t="T10000")


    def test_add_to_map(self):
        # Treba da vidimo da li ga je add_mc dobro
        # pozvao u principu. Mada mozda i da ne testiramo ovaj
        # posto je privatan i zove ga samo ovaj

        self.fail("Write maybe?")


    def test_get_maxmc(self):
        # vazno je da su dobri argumetni prosledjeni, znaci assertion error
        # iii da vrati maximalni mc,hmmm, pa hajd verovacemo mu ili ne !!!!
        with self.assertRaises(NameError):
            self.choices.get_maxmc(dir_="random",l="random",t="random")

        with self.assertRaises(AssertionError):
            self.choices.get_maxmc(dir_="random",l="L10",t="T100")


        self.choices.files = {"direktorijum":{"L10":{"T10000":{"MC100":{"THERM100":None},"MC1000":{"THERM100":None}}}}}
        with self.assertRaises(AssertionError):
            self.choices.get_maxmc(dir_="random",l="L10",t="T100")
            
        max = self.choices.get_maxmc(dir_="direktorijum",l="L10",t="T10000")
        self.assertEqual(max,1000)

    def test_choices(self):
        with self.assertRaises(AssertionError):
            self.choices.choices()

        self.choices.files = {"direktorijum":{"L10":{"T10000":{"MC100":{"THERM100":None},"MC1000":{"THERM100":None}}}}}
        with self.assertRaises(AssertionError):
            self.choices.choices(dir_="dirrr")

        with self.assertRaises(AssertionError):
            self.choices.choices(dir_="direktorijum",l="bbb")

        with self.assertRaises(AssertionError):
            self.choices.choices(dir_="direktorijum",l="L20")

        res = self.choices.choices()
        self.assertEqual(res,["direktorijum"])

            
        res = self.choices.choices(dir_="direktorijum")
        self.assertEqual(res,["L10"])

        res = self.choices.choices(dir_="direktorijum",l="L10")
        self.assertEqual(res,["T10000"])

        res = self.choices.choices(dir_="direktorijum",l="L10",t="T10000")
        self.assertEqual(res,["MC100","MC1000"])
        res = self.choices.choices(dir_="direktorijum",l="L10",t="T10000",mc="MC100")
        self.assertEqual(res,["THERM100"])
        res = self.choices.choices(dir_="direktorijum",l="L10",t="T10000",mc="MC1000")
        self.assertEqual(res,["THERM100"])


    def test_bestmat_choices(self):
        #Da su ispravnog formata i da se nalaze u mapi

        with self.assertRaises(AssertionError):
            self.choices.bestmat_choices()

        self.choices.bestmats ={"dir_":{"L10":
                                        {"T199":
                                         {"mc":"MC[100]","therm":"THERM100"},
                                         "T22":{"mc":"MC[100]","therm":"THERM100"}}}}
        res= self.choices.bestmat_choices()
        self.assertEqual(res,["dir_"])
        res = self.choices.bestmat_choices(dir_="dir_")
        self.assertEqual(res,["L10"])
        res = self.choices.bestmat_choices(dir_="dir_",l="L10")
        self.assertItemsEqual(res,["T199","T22"])
        res = self.choices.bestmat_choices(dir_="dir_",l="L10",t="T199")
        self.assertEqual(res,["MC[100]"])
        res = self.choices.bestmat_choices(dir_="dir_",l="L10",t="T22")
        self.assertEqual(res,["MC[100]"])
        res = self.choices.bestmat_choices(dir_="dir_",l="L10",t="T22",which="therm")
        self.assertEqual(res,["THERM100"])
        with self.assertRaises(AssertionError):
            res = self.choices.bestmat_choices(dir_="dir_",l="L10",t="T22",which="lala")
                         
        

    def test_get_first_static_mcs(self):
        
        self.choices.files = {"direktorijum":{"L10":{"T10000":{"MC100":{"THERM100":None},"MC1000":{"THERM100":None},"MC1000[100]":{"THERM100":None}}}}}
        res = self.choices._get_first_static_mc(dir_="direktorijum",l="L10",t="T10000")
        self.assertTrue(res in ["MC1000","MC100"])


    def test_clean_bmat(self):
        #Znaci hocu da vidim da li je clean posle clean_bmat
        def assert_dict_clean(bmat):
            for (dir_, ldict) in bmat.items():
                self.assertTrue(ldict)
                for (l, tdict) in ldict.items():
                    self.assertTrue(tdict)
                    for (t, tmc_dict) in tdict.items():
                        self.assertTrue(tmc_dict)

        bmat = {"dir_":{"L10":
                        {"T199":
                         {"mc":"MC[100]","therm":"THERM100"},
                         "T22":{"mc":"MC[100]","therm":"THERM100"}
                     }
                    }
        }
        assert_dict_clean(bmat)

        old_bmat = bmat
        self.choices._clean_bmat(bmat)
        self.assertEqual(old_bmat,bmat)
        assert_dict_clean(bmat)

        bmat = {"dir_":{"L10":
                        {"T199":
                         {"mc":"MC[100]","therm":"THERM100"},
                         "T22":{"mc":"MC[100]","therm":"THERM100"}
                     },"L20":{}
                    }
        }
        self.choices._clean_bmat(bmat)
        self.assertEqual(old_bmat,bmat)
        assert_dict_clean(bmat)
        bmat = {"dir_":{"L10":
                        {"T199":
                         {"mc":"MC[100]","therm":"THERM100"},
                         "T22":{"mc":"MC[100]","therm":"THERM100"},
                         "T30":{}
                     },"L20":{}
                    },
                "hmm_":{}
        }
        self.choices._clean_bmat(bmat)
        self.assertEqual(old_bmat,bmat)
        assert_dict_clean(bmat)
        


    def test_load_choices(self):
        with self.assertRaises(TypeError):
            self.choices.load_choices(1)

        with self.assertRaises(TypeError):
            self.choices.load_choices(dict())


    

    def test_dist_by_components(self):
        self.fail("Write function???")

    def test_load_sp_data(self):
        with self.assertRaises(TypeError):
            self.choices.load_sp_data("str","str","str")
        with self.assertRaises(AssertionError):
            self.choices.load_sp_data("str","str",1)
        self.load_files()
        self.load_bestmats_()

        data = self.choices.load_sp_data("g0","L10",1)
        self.fail(str(data))
        
        
#        self.fail("Write test")
    def test_unify(self):
        self.fail("Write test")


    def test_spify(self):
        self.fail("Write test")


    def test_therm_count(self):
        self.fail("Write test")


    def load_files(self):
        self.choices.files = {"g0":{"L10":{"T10000":{"MC1000":{"THERM1000":None},"MC1000":{"THERM1000":None}}}}}
        

    def load_bestmats_(self):
        self.choices.bestmats ={"g0":{"L10":
                                       {
                                         "T10000":{"mc":"MC1000","therm":"THERM1000"}}}}
        

    def test_name_gen(self):
        self.fail("Write test")

    def test_read_tdfile(self):
        self.fail("Write test")

    def test_mag_index(self):
        self.fail("Write test")


    def test_mag_components(self):
        self.fail("Write test")


    def test_mag_index(self):

        self.fail("Write tests")

    def test_calculate_magt(self):

        self.fail("Write tests")

    def test_get_filename(self):

        self.fail("Write tests")
        
    def test_create_mat(self):

        self.fail("Write tests")
        
    def test_compose(self):

        self.fail("Write tests")
        
    def test_get_plot_dict(self):

        self.fail("Write tests")

    def test_agregate(self):

        self.fail("Write tests")
        
    def test_agg(self):

        self.fail("Write tests")

    def test_remap_fsystem(self):

        self.fail("Write tests")

    def test_load_bestmat(self):

        self.fail("Write tests")

    def test___make_consistent(self):

        self.fail("Write tests")

    def test_map_filesystem(self):

        self.fail("Write tests")
        
            
        
        
unittest.main()










